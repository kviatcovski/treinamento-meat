export interface Restaurant {
    id: string
    name: string
    category: string
    deliveryEstimate: string
    rating: number
    imagePath: string 
} //model igual a pessoa que fizemos no teste