import { Component, OnInit } from '@angular/core';
import { RestaurantsService } from 'app/restaurants/restaurant/restaurants.service';
import { Restaurant } from 'app/restaurants/restaurant/restaurant.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'mt-restaurant-detail',
  templateUrl: './restaurant-detail.component.html',
})
export class RestaurantDetailComponent implements OnInit {
  restaurant: Restaurant;
  id: string;

  constructor (private restaurantsService: RestaurantsService,
               private route: ActivatedRoute) {

               }

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];

    this.restaurantsService.restaurantById(this.id)
    .subscribe(restaurant => this.restaurant = restaurant)


    console.log(this.restaurantsService.restaurantById(this.id));

  }

}
